/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  ImageBackground,
  Image
} from 'react-native';

import 'react-native-gesture-handler';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionSpecs, HeaderStyleInterpolators } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Icon } from 'react-native-elements';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';

const config = {
  gestureDirection: 'horizontal',
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec
  },
  headerStyleInterpolator: HeaderStyleInterpolators.forFade,
  cardStyleInterpolator: ({ current, next, layouts }) => {
    return {
      cardStyle: {
        transform: [
          {
            translateX: current.progress.interpolate({
              inputRange: [0,1],
              outputRange: [layouts.screen.width+250, 0],
            })
          },
          {
            rotate: current.progress.interpolate({
              inputRange: [0,1],
              outputRange: [1,0],
            })
          },
          {
            scale: next? next.progress.interpolate({
              inputRange: [0,1],
              outputRange: [0,0.5]
            }):1
          },
        ]
      },
      overlayStyle: {
        opacity: current.progress.interpolate({
          inputRange: [0,1],
          outputRange: [0,0.5]
        })
      }
    }
  }
};

class Beranda extends Component{
  render(){
    const{navigate} = this.props.navigation;
    return(
      <View style={styles.content}>
        <Text>Beranda</Text>
        <Button style={styles.button} title='Ayo Latihan' onPress={()=>navigate('Soal')}/>
      </View>
    );
  }
}

class Soal extends Component{
  render(){
    const{navigate} = this.props.navigation;
    return(
      <View style={styles.context}>
        <Text>Ini Soal Latihannya</Text>
        <Button title='KEMBALI KE BERANDA' onPress={()=>navigate('Beranda')}/>
      </View>
    );
  }
}

const properti = [
  {
    id: '1',
    title: 'sunglasses'
  },
  {
    id: '2',
    title: 'shorts'
  }
]

function Item({ id, title, selected, onSelect }) {
  return (
    <TouchableOpacity
      onPress={() => onSelect(id)}
      style={[
        styles.item,
        { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
      ]}
    >
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
}

class Profil extends Component{
  static navigationOptions = {
    title : 'Profil',
    tabBarColor: '#e74c3c',
    tabBarIcon: <Icon name='person' color='#000' size={30}/>
  }
  state = {
    isWearingSunglasses: false,
    isWearingShorts: false,
  }
  wearSunglasses = () => {
    this.setState({isWearingSunglasses: !this.state.isWearingSunglasses});
  }
  wearShorts = () => {
    this.setState({isWearingShorts: !this.state.isWearingShorts});
  }
  // const [selected, setSelected] = React.useState(new Map());
  // const onSelect = React.useCallback(
  //   id=>{
  //     const newSelected = new Map(selected);
  //     newSelected.set(id, !selected.get(id));
  //     setSelected(newSelected);
  //   },
  //   [selected],
  // );
  render(){
    return(
      <View style={styles.profil}>
        <ImageBackground source={require('./assets/character.png')} style={styles.image}>
          <Image source={(this.state.isWearingSunglasses)?require('./assets/sunglasses.png'):0} style={styles.sunglasses}/>
          <Image source={(this.state.isWearingShorts)?require('./assets/shorts.png'):null} style={styles.clothes}/>
        </ImageBackground>
        {/* <FlatList data={properti} renderItem={({item})=>(
          <Item
            id={item.id}
            title={item.title}
            selected={!!selected.get(item.id)}
            onSelect={onSelect}
          />
        )}
        /> */}
        {/* <TouchableOpacity style={styles.btnKacamata}>
          <Image source={require('./assets/sunglasses.png')} style={styles.btnSunglasses}/>
        </TouchableOpacity> */}
        <Button title='Kacamata' onPress={
          ()=>this.wearSunglasses()
        }/>
        <Button title='Celana' onPress={
          ()=>this.wearShorts()
        }/>
      </View>
    );
  }
}

class Toko extends Component{
  static navigationOptions = {
    title : 'Toko',
    headerTransparent: true,
    headerTintColor: '#000',
    headerStyle:{
      backgroundColor: 'rgba(0,0,0,0.3)'
    },
    tabBarColor: '#f1c40f',
    tabBarIcon: <Icon name='store' color='#000'  size={30}/>
  }
  render(){
    return(
      <View style={styles.content}>
        <Text>Toko</Text>
      </View>
    );
  }
}

const tabNavigator = createStackNavigator({
  Beranda: {screen: Beranda},
  Soal: {screen: Soal}
},
{
  defaultNavigationOptions: {
    headerShown: false,
    ...config
  }
})

tabNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  let tabBarColor= '#e67e22';
  let tabBarIcon = <Icon name='home' color='#000'/>
  if(navigation.state.index > 0){
    tabBarVisible = false;
  }
  return{
    tabBarVisible,
    tabBarColor,
    tabBarIcon
  };
};

const styles = StyleSheet.create({
  content:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button:{
    justifyContent: 'center',
    color: '#3498db'
  },
  profil:{
    flex: 1,
    marginTop: 10,
  },
  image:{
    width: '100%',
    height: '75%',
  },
  sunglasses:{
    width: '100%',
    height: '30%',
    resizeMode: "contain",
    marginTop: 13.5
  },
  clothes:{
    width: '100%',
    height: '20%',
    resizeMode: 'contain',
    marginTop: 95
  },
  btnSunglasses:{
    width: '100%',
    height: '50%',
    resizeMode: "contain",
  },
  item: {
    backgroundColor: '#f9c2ff',
    marginVertical: 8,
    marginHorizontal: 16,
  },
});

const App = createAppContainer(createMaterialBottomTabNavigator({
  Beranda: {screen: tabNavigator},
  Profil: {screen: Profil},
  Toko: {screen : Toko}
},{
  backBehavior: 'order',
  activeColor: '#000',
  barStyle: { 
    backgroundColor: '#34495e',
  },
  shifting: true
}));

export default App;
