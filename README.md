#Slideable Bottom Bar Navigation

First of all, it is not a real `bottomBarNavigation`. The Bottom Bar Navigation cannot be swipe as `swipeEnabled` was deprecated in `BottomTabNavigatorConfig v4.x` <br>
So, I made a research and all you have to do is make a top bar navigation as the top bar navigation default `swipeEnabled` is true and make the `tabBarPosition : bottom`. <br>
Here is the demo of the app : <br>
![456312.t](/uploads/e64dcc3fc6dff8a6f63d60123de09f26/456312.t.mp4)